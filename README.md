# tesseract.js-core


Core part of tesseract.js, which compiles original tesseract from C to JavaScript WebAssembly.

# Environment
- Emscripten: 1.38.16 (trzeci/emscripten:sdk-tag-1.38.16-64bit)
- Leptonica: 1.74.2
- zlib: 1.2.5
- libtiff: 3.9.4
- libjpeg: 8.4.0
- libpng: 1.4.22
- Tesseract: 4.0.0

# Contribution

As I included all submodules to as folders, you don't need to add recursive when cloning the repository:

$ git clone https://github.com/naptha/tesseract.js-core  (--recursive x )

To build tesseract-core.js by yourself, please install docker and run:

$ sh buid.sh

The genreated files will be stored in src/tesseract-core.*
